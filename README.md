Git Commit time track
=====================

heuristic for estimating the time spent using git commits times.

generate data for this script:

[download](https://gitlab.com/csicar/gittimetrack/-/jobs/74153754/artifacts/file/dist/dist-sandbox-756f5739/build/gittimetrack/gittimetrack)

```bash
$ git log --format=\"%at\" > timelog.txt
```

run script with `timelog.txt` in the same directory:
```bash
$ ./main.hs
```


