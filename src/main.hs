#!/bin/runhaskell
module Main where

import Prelude 
import Data.String (lines)
import Control.Monad (forM_)
import Data.Time.Clock.POSIX
import Data.Time.Format
import System.Process (callCommand)
import Data.List (sortBy, intersperse)
import Data.Monoid ((<>))
import Options.Applicative (Parser, ParserInfo, argument, auto, execParser,
                           fullDesc, header, help, helper, info, long, metavar,
                           option, progDesc, short, str, value, (<$>))

data Worktime = Worktime 
   { commits:: [Integer]
   , start:: Integer
   , end:: Integer
   }

truncate' :: Int -> Double -> Double
truncate' n x = (fromIntegral (floor (x * t))) / t
   where t = 10^n

showTime = formatTime defaultTimeLocale  "%c" . posixSecondsToUTCTime . fromInteger

fromIntList :: [Integer] -> Worktime
fromIntList commits = Worktime commits (minimum commits) (maximum commits)

duration :: Integer -> Worktime -> Integer
duration minMinutes (Worktime _ start end) = end - start + minMinutes*60

durationToHours :: Integer -> Double
durationToHours d = truncate' 2 $ fromInteger d / (60*60)

showDuration :: Integer -> String
showDuration = (++"h") . show . durationToHours

showWorktime minMinutes w@(Worktime commits start end) = concat 
   [ "von "
   , showTime start
   , " bis "
   , showTime end
   , " duration "
   , show $ durationToHours $ duration minMinutes w
   , "h \n  "
   , show $ showTime <$> commits
   ]

sumWorktimes :: Integer -> [Worktime] -> Integer
sumWorktimes minMinutes = sum . map (duration minMinutes)

sortByDiffs = sortBy f
   where f (diffA, _, _) (diffB, _, _) = compare diffA diffB

main = do
   --callCommand "git log --format=\"%at\" > timelog.txt"
   opts <- execParser optsParserInfo
   file <- readFile (filename opts)
   let 
      dates = map read $ lines file
      duration' = duration (commitTime opts)
      sumWorktimes' = sumWorktimes (commitTime opts)
   let gs =  (\it -> (it, connectTimes it dates)) <$> [(minInterval opts)*60, 45*60 .. (maxInterval opts)*60]
   let diffGs = zipWith (\(intA, gA) (intB, gB) -> ((sumWorktimes' gB)-(sumWorktimes' gA), intA, gA)) gs (tail gs)
   let sorted = reverse $ sortByDiffs diffGs

   putStrLn "diff:\t Difference in worktimeHours between an interval of interval and interval-15min"
   putStrLn "intvl:\t Maximum time difference still considered continuos work"
   putStrLn "worktimeHours:\t calculated worktime for the given interval"
   putStrLn ""

   putStrLn $ concat $ intersperse "\t\t" $
      [ "sum", "intvl", "diff"]

   forM_ sorted $ \(diff, interval, g) -> do
      putStrLn $ concat $ intersperse "\t\t" $ 
         [ showDuration $ sumWorktimes' g
         , showDuration interval
         , showDuration diff
         ]
      

connectTimes :: Integer -> [Integer] -> [Worktime]
connectTimes maxTime = map fromIntList . snd . foldr f (0, [])
      where
         f commitTime (lastTime, grouped) = 
            let
               timeDiff = commitTime - lastTime
            in
               if timeDiff > maxTime then
                  (commitTime, grouped ++ [[commitTime]])
               else 
                  (commitTime, insertAtLast commitTime grouped)

insertAtLast :: a -> [[a]] -> [[a]]
insertAtLast a [] = [[a]]
insertAtLast a [x] = [x++[a]]
insertAtLast a (x:xs) = x : insertAtLast a xs

data Options = Options
   { filename :: String
   , commitTime :: Integer
   , minInterval :: Integer
   , maxInterval :: Integer
   }

optsParser :: Parser Options
optsParser = Options
   <$> argument str
      ( metavar "FILENAME"
      <> help "Input Filename"
      <> value "timelog.txt"
      )
   <*> option auto
      ( metavar "COMMITINTERVAL"
      <> short 'n'
      <> long "commit-interval"
      <> help "minimum time for a commit in minutes"
      <> value 30
      )
   <*> option auto
      ( metavar "MININTERVAL"
      <> help "minimum connection interval to try in minutes"
      <> long "min-interval"
      <> value 30
      )
   <*> option auto
      ( metavar "MAXINTERVAL"
      <> help "maximum connection interval to try in minutes"
      <> long "max-interval"
      <> value (4*60)
      )
   

optsParserInfo :: ParserInfo Options
optsParserInfo = info (helper <*> optsParser)
   (  fullDesc
   <> progDesc "calculate worktime from commits"
   <> header "gittimetrack"
   )